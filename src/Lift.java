import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Lift {

    private static final int LIFTS = 3;

    public static void main(String[] args) {

        ExecutorService lifts = Executors
                .newFixedThreadPool(LIFTS);

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int[] floors = new int[n];
        String[] names = new String[n];

        for (int i = 0; i < n; i++) {
            System.out.println("Enter a person name for entry : " + i);
            names[i] = sc.next();
            System.out.println("Enter a floor name for entry : " + i);
            floors[i] = sc.nextInt();
        }

        for (int i = 0; i < n; i++) {
            Runnable worker = new People(floors[i], names[i]);
            lifts.execute(worker);
        }

//        lifts.shutdown();
        while (!lifts.isTerminated()) {
        }
        lifts.shutdown();

        System.out.println("Finished all lifts :)");

    }

    public static class People implements Runnable {

        private int floor;
        private final String person;
        private int count = 0;

        @Override
        public void run() {

            System.out.println("Lift " + SplitName.splitThread(Thread.currentThread().getName()) +
                    " is allotted for Person " + person +
                    " who wants to got to " + floor + " floor");

            while (floor > count) {
                goUp();
                System.out.println("Lift " + SplitName.splitThread(Thread.currentThread().getName()) + " is in " +
                        count + " floor :)");
            }
//            while (!(floor == 0)) {
//                goDown();
//                System.out.println("Lift is in " + Thread.currentThread().getName() +
//                        count + " floor :)");
//                floor--;
//            }
        }

        private void goDown() {
            count--;
        }

        private void goUp() {
            count++;
        }

        public People(int floor, String person) {
            this.floor = floor;
            this.person = person;
//            Thread t = new Thread();
//            t.run();
        }
    }
}
