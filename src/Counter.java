import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Counter {

        private static final int COUNTER = 4;

        public static void main(String args[]) throws Exception {

            ExecutorService counter = Executors.newFixedThreadPool(COUNTER);
            Scanner sc = new Scanner(System.in);

            System.out.println("Enter the number of customers : ");

            int n = sc.nextInt();

            int[] time = new int[n];
            String[] names = new String[n];

            for (int i = 0; i < n; i++) {
                System.out.println("Enter the name of the person for entry : " + i);
                names[i] = sc.next();
                System.out.println("Enter the time for the person for entry : " + i);
                time[i] = sc.nextInt();
            }

            for (int i = 0; i < n; i++) {
                Runnable worker = new CustomerProcess(time[i], names[i]);
                counter.execute(worker);
            }

            counter.shutdown();
            System.out.println("Finished all counters :)");
        }

        public static class CustomerProcess implements Runnable {

            private final int time;

            private final String customerName;

            CustomerProcess(int time, String name) {
                this.time = time;
                this.customerName = name;
//                Thread t = new Thread();
//                t.run();
            }

            @Override
            public void run() {
                try {
                    System.out.println("Customer " + customerName + " has started to get executed in Counter " +
                            SplitName.splitThread(Thread.currentThread().getName()));
                    Thread.sleep(time);
                    System.out.println("Task got completed in " + time + " milliseconds " +
                            "for Customer " + customerName + " in Counter " +
                            SplitName.splitThread(Thread.currentThread().getName()));

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

